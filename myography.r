#
#    Processing of myoelectric signals using data collected from the
#    anterior forearm (radial and ulnar regions)
#
#    Copyright (C) 2015  
#
#    Authors:
#
#	Alberto Monteiro Peixoto
#	Alexandre Delbem
#	Edwin Villanueva Talavera
#	Leandro Brito Santos
#	Martha Dais Ferreira
#	Renelson Ribeiro Sampaio
#	Roberto Luis Souza Monteiro
#	Rodrigo Mello
#	Soledad Espezua
#	Yule Vaz
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require(signal)

# gama = [38, 1023]
# high beta = [30, 38]
# beta = [16, 20] *
# low beta = [13, 16]
# alpha = [8, 13]
# theta = [4, 8]
# delta = [0, 4]

guru <- function(signal, min.freq = 15, max.freq = 17, fft.window = 128, fft.step = 64, samp.rate = 256, plot = F) {

	new.signal = c()
	id = seq(1, length(signal)-fft.window-1, by=fft.step)

	for (i in 1:(length(id)-1)) {
		chirp = signal[id[i]:(id[i]+fft.window)-1]
		window = chirp #c(rep(0, fft.window/2), chirp, rep(0, fft.window/2))

		# chebyshev windowing function
		windowing.function = chebwin(length(window),100)

		# fft
		coeffs = fft(window) # * windowing.function)

		if (plot == T) {
			plot(sqrt(Re(coeffs)^2+Im(coeffs)^2))
			locator(1)
		}

		# cutting frequencies
		freq.seq = seq(1, length(window), by=1)
		freq.ids1 = which(freq.seq >= min.freq & freq.seq <= max.freq)
		freq.ids2 = which(freq.seq >= (length(window) - max.freq) & freq.seq <= (length(window) - min.freq))
		freqs = c(freq.ids1, freq.ids2)
		coeffs[-freqs] = 0

		if (plot == T) {
			plot(sqrt(Re(coeffs)^2+Im(coeffs)^2))
			locator(1)
		}

		# inversing
		inv.coeffs = fft(coeffs, inverse=T)
		new.window = Re(inv.coeffs) / length(window)

		new.signal = c(new.signal, new.window)
	}

	new.signal
}

guru.window <- function(chirp, min.freq = 15, max.freq = 17) {

	window = chirp

	# chebyshev windowing function
	windowing.function = chebwin(length(window),100)

	# fft
	coeffs = fft(window) # * windowing.function)

	# cutting frequencies
	freq.seq = seq(1, length(window), by=1)
	freq.ids1 = which(freq.seq >= min.freq & freq.seq <= max.freq)
	freq.ids2 = which(freq.seq >= (length(window) - max.freq) & freq.seq <= (length(window) - min.freq))
	freqs = c(freq.ids1, freq.ids2)
	coeffs[-freqs] = 0

	# inversing
	inv.coeffs = fft(coeffs, inverse=T)
	new.window = Re(inv.coeffs) / length(window)

	new.window
}

read.signal <- function(input.file, min.freq = 15, max.freq = 17, 
			fft.window = 128, fft.step = 64, 
			samp.rate = 256, alpha = 0.125, 
			mult.factor = 0.25, plot = F, column.id = 5,
			n.peaks = 256) {

	force.max = 1
	ic = 0

	th = 0
	threshold.signal = NULL
	signal.buffer = NULL
	new.signal = NULL
	peaks.max = rep(0, n.peaks)

	conn <- file(input.file, "r", blocking = F)

	par(mfrow=c(2,1))
	plot.new()
	plot.new()
	locator(1)

	while (TRUE) {
		vec = readLines(conn, 1)

		if (length(vec) != 0) {

			vec = as.numeric(strsplit(vec, split=",")[[1]][column.id])
			signal.buffer = c(signal.buffer, vec)

			if (length(signal.buffer) > fft.window + fft.step) {
				start = length(signal.buffer)-fft.window+1
				end = length(signal.buffer)
				signal.buffer = signal.buffer[start:end]

				curr.signal = guru.window(signal.buffer, min.freq, max.freq)
				new.signal = c(new.signal, curr.signal)

				# moving average
				energy = curr.signal^2
	
				for (j in 1:length(energy)) {
					ids = which(energy[j] > peaks.max)
					if (length(ids) > 0) {
						id = which.min(peaks.max)
						peaks.max[id] = energy[j]
					}
				}
				th = (1-alpha)*th + alpha*(mult.factor*mean(peaks.max))

				# computing force
				force = 0
				if (max(energy) - th > 0) {
					cat("hand is closed.\n")
					force = sum(energy)
				} else {
					cat("hand is NOT closed\n")
				}

				if (force > force.max) force.max = force

				relative.force = c((force/force.max)*100)

				#cat("Force: ", force, " Force.max: ", 
				#	force.max, "Relative: ", relative.force, "\n")

				if (plot == T) {
					ts.plot(new.signal, main="Sensor channel")
					barplot(relative.force, xlim=c(0, 100), 
						horiz=TRUE, main="Force intensity")
					#locator(1)
					#ts.plot(new.signal^2)
					#lines(threshold.signal, col=2)
				}
			}
		}
	}

}






